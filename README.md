# docker-compose
[![Pipeline Status](https://gitlab.com/ix.ai/docker-compose/badges/master/pipeline.svg)](https://gitlab.com/ix.ai/docker-compose/)
[![Docker Stars](https://img.shields.io/docker/stars/ixdotai/docker-compose.svg)](https://hub.docker.com/r/ixdotai/docker-compose/)
[![Docker Pulls](https://img.shields.io/docker/pulls/ixdotai/docker-compose.svg)](https://hub.docker.com/r/ixdotai/docker-compose/)
[![Docker Image Version (latest)](https://img.shields.io/docker/v/ixdotai/docker-compose/latest)](https://hub.docker.com/r/ixdotai/docker-compose/)
[![Gitlab Project](https://img.shields.io/badge/GitLab-Project-554488.svg)](https://gitlab.com/ix.ai/docker-compose/)

Multi-arch build of [docker/compose](https://github.com/docker/compose)

This is only a pipeline running against [docker/compose](https://github.com/docker/compose) and pushes to [registry.gitlab.com/ix.ai/docker-compose](https://gitlab.com/ix.ai/docker-compose/container_registry) and [ixdotai/docker-compose](https://hub.docker.com/r/ixdotai/docker-compose).

## Docker Registries

This image is hosted on both GitLab and Docker Hub:
* `registry.gitlab.com/ix.ai/docker-compose`
* `ixdotai/docker-compose`

## Docker Image Tags

* `registry.gitlab.com/ix.ai/docker-compose:master` is used for the current [master](https://github.com/docker/compose/tree/master) branch
* `registry.gitlab.com/ix.ai/docker-compose:n.nn.nn` is used for the current [n.nn.nn](https://github.com/docker/compose/releases) tag release
* `registry.gitlab.com/ix.ai/docker-compose:n.nn.nn-rc*` is used for the [release candidates](https://github.com/docker/compose/releases)
* `registry.gitlab.com/ix.ai/docker-compose:latest` always reflects the last `n.nn.nn` tag pushed

## Frequency

The `registry.gitlab.com/ix.ai/docker-compose:master` tag is rebuilt on a daily schedule, based on the latest [master](https://github.com/docker/compose/tree/master) commit. The builds for `registry.gitlab.com/ix.ai/docker-compose:n.nn.nn` and `registry.gitlab.com/ix.ai/docker-compose:n.nn.nn-rc*` are triggered manually, on git push of the tag in this repository.
