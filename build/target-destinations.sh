#!/usr/bin/env sh

set -e

TAGS=""

_add_tag_latest(){
  if [ ! "${CI_COMMIT_REF_NAME}" = "master" ]; then
    if ! expr "${CI_COMMIT_REF_NAME}" : "-rc" 1>/dev/null; then
      TAGS="${TAGS} ${1}:latest"
    fi
  fi
}

# GitLab Registry
TAGS="${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}"
_add_tag_latest "${CI_REGISTRY_IMAGE}"

# Docker Hub
if [ -n "${DOCKERHUB_USERNAME}" ] && [ -n "${DOCKERHUB_PASSWORD}" ]; then
  TAGS="${TAGS} ${DOCKERHUB_REPO_PREFIX}/${DOCKERHUB_REPO_NAME}:${CI_COMMIT_REF_NAME}"
  _add_tag_latest "${DOCKERHUB_REPO_PREFIX}/${DOCKERHUB_REPO_NAME}"
else
  echo "ERROR! DOCKERHUB_USERNAME or DOCKERHUB_PASSWORD environment variables missing"
fi

for TAG in ${TAGS}; do
  DESTINATIONS="${DESTINATIONS} --tag ${TAG}"
done

echo "${DESTINATIONS}" > "/tmp/${CI_PROJECT_NAME}-${CI_COMMIT_SHA}-destinations"
