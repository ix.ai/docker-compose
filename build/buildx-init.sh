#!/usr/bin/env sh

set -e

BUILDX_NAME="buildx-${CI_COMMIT_SHA}"

docker context create "${BUILDX_NAME}"

update-binfmts --enable # Important: Ensures execution of other binary formats is enabled in the kernel

docker buildx create --driver docker-container \
        --name "${BUILDX_NAME}" \
        --use "${BUILDX_NAME}"
docker buildx inspect --bootstrap
docker buildx ls
